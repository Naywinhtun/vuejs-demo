import Vue from 'vue'
import Router from 'vue-router'
// import axios from '../../src/axios-course'

import Posts from '@/components/Posts/Posts'
import Create from '@/components/Posts/Post/Create'
import Edit from '@/components/Posts/Post/Edit'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Posts',
      component: Posts
    },
    {
      path: '/new-post',
      name: 'Create',
      component: Create
    },
    {
      path: '/post/:id/edit',
      name: 'Edit',
      component: Edit
    }
  ]
})
