import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://vue-app-ffd3c.firebaseio.com/'
})

export default instance
